<?php

namespace Symfony\Component\Routing\Loader\Configurator;

return function (RoutingConfigurator $routes) {
    $routes->add('data', ['pl' => '/', 'en' => '/'])
        ->controller('App\Controller\DataController::index');
};