<?php
/**
 * Created by PhpStorm.
 * User: softwellhouse
 * Date: 25.07.2018
 * Time: 18:41
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class DataController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $request = Request::createFromGlobals();
        $siteUrl = trim($request->query->get('url'));
        $fileUrl = trim($request->query->get('name'));
        $imageList = [];
        if (isset($siteUrl) && !empty($siteUrl)) {
            $url = rtrim(urldecode($siteUrl), '/');

            $imageList = $this->getImages($url);
        } elseif (isset($fileUrl) && !empty($fileUrl)) {
            $fileUrl = rtrim(urldecode($fileUrl), '/');
            $path_parts = pathinfo($fileUrl);
            $imageList[] = [
                'dirname' => $path_parts['dirname'],
                'basename' => $path_parts['basename'],
                'extension' => (isset($path_parts['extension'])) ? $path_parts['extension'] : null,
                'filename' => $path_parts['filename'],
                'fileChangeTime' => $this->getTimeChange($fileUrl),
                'url' => $fileUrl,
            ];
        }
        return $this->render('data/form.twig', [
            'ImageList' => $imageList,
            'Name' => $fileUrl,
            'Url' => $siteUrl,
        ]);
    }

    /**
     * @param null $url
     * @return array|null
     */
    private function getImages($url = null)
    {
        if (is_null($url)) {
            return NULL;
        }
        $html = file_get_contents($url);
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        $images = $dom->getElementsByTagName('img');
        $ret = [];
        foreach ($images as $image) {
            $imageUrl = $url . $image->getAttribute('src');
            $path_parts = pathinfo($imageUrl);
            if ($path_parts['basename'] !== 'category') {
                $ret[] = [
                    'dirname' => $path_parts['dirname'],
                    'basename' => $path_parts['basename'],
                    'extension' => (isset($path_parts['extension'])) ? $path_parts['extension'] : null,
                    'filename' => $path_parts['filename'],
                    'fileChangeTime' => $this->getTimeChange($imageUrl),
                    'url' => $imageUrl,
                ];
            }
        }
        return $ret;
    }

    public function checkAddress($url)
    {
        if (strpos($url, "http://") !== false) {
            return $url;
        } elseif (strpos($url, "https://") !== false) {
            return $url;
        } else {
            return "http://" . $url;
        }

    }

    private function getTimeChange($urlFile)
    {
        $curl = curl_init($urlFile);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FILETIME, true);
        $result = curl_exec($curl);
        if ($result === false) {
            return NULL;
        }
        $timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
        if ($timestamp != -1) {
            return date("Y-m-d H:i:s", $timestamp);
        }
    }
}