<?php

/* data/form.twig */
class __TwigTemplate_00d14bbf9b7f784159bab60c4e5673f6d1511b2e87e909da9b04e2f5d14e49d4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "data/form.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "data/form.twig"));

        // line 1
        echo "<section id=\"check-file\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <form method=\"get\">
                <label for=\"url_address\">Podaj adres strony by zobaczyć listę obrazków</label>
                <input id=\"url_address\" placeholder=\"Adres strony\" type=\"url\" name=\"url\" style=\"width: 20%;\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["Url"]) || array_key_exists("Url", $context) ? $context["Url"] : (function () { throw new Twig_Error_Runtime('Variable "Url" does not exist.', 6, $this->source); })()), "html", null, true);
        echo "\" />
                lub
                <label for=\"url_file\">Podaj pełny adres obrazka</label>
                <input id=\"url_file\" placeholder=\"Pełnu adres pliku\" type=\"url\" name=\"name\" style=\"width: 20%;\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["Name"]) || array_key_exists("Name", $context) ? $context["Name"] : (function () { throw new Twig_Error_Runtime('Variable "Name" does not exist.', 9, $this->source); })()), "html", null, true);
        echo "\" />
                <br />
                <button type=\"submit\" class=\"btn-success\">
                    Pobierz dane
                </button>
            </form>
        </div>
    </div>
    <table border=\"1px solid #000;\">
        <thead>
        <tr>
            <td>
                Czas zmiany pliku
            </td>
            <td>
                Nazwa pliku
            </td>
            <td>
                Rozszeżenie pliku
            </td>
            <td>
                Nazwa pliku
            </td>
            <td>
                Folder
            </td>
            <td>
                Url pliku
            </td>
        </tr>
        </thead>
        <tbody>
        ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["ImageList"]);
        foreach ($context['_seq'] as $context["_key"] => $context["ImageList"]) {
            // line 42
            echo "
            <tr>
                <td>
                    ";
            // line 45
            if ( !(null === twig_get_attribute($this->env, $this->source, $context["ImageList"], "filetimechange", array()))) {
                // line 46
                echo "                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ImageList"], "filetimechange", array()), "html", null, true);
                echo "
                    ";
            } else {
                // line 48
                echo "                        Nie odnaleziono
                    ";
            }
            // line 50
            echo "                </td>
                <td>
                    ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ImageList"], "basename", array()), "html", null, true);
            echo "
                </td>
                <td>
                    ";
            // line 55
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ImageList"], "extension", array()), "html", null, true);
            echo "
                </td>
                <td>
                    ";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ImageList"], "filename", array()), "html", null, true);
            echo "
                </td>
                <td>
                    ";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ImageList"], "dirname", array()), "html", null, true);
            echo "
                </td>
                <td>
                    ";
            // line 64
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ImageList"], "url", array()), "html", null, true);
            echo "
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ImageList'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "
        </tbody>
    </table>
</section>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "data/form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 68,  126 => 64,  120 => 61,  114 => 58,  108 => 55,  102 => 52,  98 => 50,  94 => 48,  88 => 46,  86 => 45,  81 => 42,  77 => 41,  42 => 9,  36 => 6,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"check-file\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <form method=\"get\">
                <label for=\"url_address\">Podaj adres strony by zobaczyć listę obrazków</label>
                <input id=\"url_address\" placeholder=\"Adres strony\" type=\"url\" name=\"url\" style=\"width: 20%;\" value=\"{{ Url }}\" />
                lub
                <label for=\"url_file\">Podaj pełny adres obrazka</label>
                <input id=\"url_file\" placeholder=\"Pełnu adres pliku\" type=\"url\" name=\"name\" style=\"width: 20%;\" value=\"{{ Name }}\" />
                <br />
                <button type=\"submit\" class=\"btn-success\">
                    Pobierz dane
                </button>
            </form>
        </div>
    </div>
    <table border=\"1px solid #000;\">
        <thead>
        <tr>
            <td>
                Czas zmiany pliku
            </td>
            <td>
                Nazwa pliku
            </td>
            <td>
                Rozszeżenie pliku
            </td>
            <td>
                Nazwa pliku
            </td>
            <td>
                Folder
            </td>
            <td>
                Url pliku
            </td>
        </tr>
        </thead>
        <tbody>
        {% for ImageList in ImageList %}

            <tr>
                <td>
                    {% if ImageList.filetimechange is not null %}
                        {{ ImageList.filetimechange }}
                    {% else %}
                        Nie odnaleziono
                    {% endif %}
                </td>
                <td>
                    {{ ImageList.basename }}
                </td>
                <td>
                    {{ ImageList.extension }}
                </td>
                <td>
                    {{ ImageList.filename }}
                </td>
                <td>
                    {{ ImageList.dirname }}
                </td>
                <td>
                    {{ ImageList.url }}
                </td>
            </tr>
        {% endfor %}

        </tbody>
    </table>
</section>", "data/form.twig", "/Applications/MAMP/htdocs/test1/test1/templates/data/form.twig");
    }
}
