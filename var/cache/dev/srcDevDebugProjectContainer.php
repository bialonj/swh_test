<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerMTxAgzn\srcDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerMTxAgzn/srcDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerMTxAgzn.legacy');

    return;
}

if (!\class_exists(srcDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerMTxAgzn\srcDevDebugProjectContainer::class, srcDevDebugProjectContainer::class, false);
}

return new \ContainerMTxAgzn\srcDevDebugProjectContainer(array(
    'container.build_hash' => 'MTxAgzn',
    'container.build_id' => 'd5db43d8',
    'container.build_time' => 1532595575,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerMTxAgzn');
